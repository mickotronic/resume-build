require "yaml"

class Config

  ROOT_DIR = File.expand_path("../",File.dirname(__FILE__))
  YAML_SOURCE = "#{ROOT_DIR}/data"
  LOCAL_CONFIG_FILE = "config.yml" 
  DEFAULT_TEMPLATE = "templates/default.haml" 
  DEFAULT_STYLESHEET = "stylesheets/default.css"

  def initialize()
    @local_config = read_local_config()
  end

  def template_config
    @local_config&.dig("template_config")
  end

  def local_config_file
    File.expand_path("#{ROOT_DIR}/#{LOCAL_CONFIG_FILE}", File.dirname(__FILE__))
  end

  def read_local_config()
    return unless File.exist?(local_config_file)

    yaml = YAML.load_file(local_config_file)

    if yaml == false
      return {}
    else
      return yaml
    end
  end

  def yaml_source
    ENV["YAML_SOURCE"] || @local_config&.dig("yaml_source") || YAML_SOURCE
  end

  def yaml_source_include
    @local_config&.dig("yaml_source_include")
  end

  def template_file
    template = ENV["RESUME_TEMPLATE"] || @local_config&.dig("template") || DEFAULT_TEMPLATE
    template = "#{ROOT_DIR}/#{template}"
    template
  end

  def read_template()
    return nil unless File.exist?(template_file)

    File.read(template_file)
  end

  def template
    read_template() 
  end

  def stylesheet_file
    ENV["RESUME_STYLESHEET"] || @local_config&.dig("stylesheet")  || DEFAULT_STYLESHEET
  end

  
  def yaml_source_dir_exists?
    Dir.exist?(yaml_source)
  end

  def template_file_exists?
    File.exist?(template_file)
  end

  def stylesheet_file_exists?
    File.exist?(stylesheet_file)
  end

  def filter_yaml_files
    source_yaml_files = Dir.glob(File.join(yaml_source, "*.{yml,yaml}"))
    target_yaml_files = []

    source_yaml_files.each do |f|
      if yaml_source_include.nil? or yaml_source_include.include? File.basename(f)
        target_yaml_files.push(f)
      end
    end

    return target_yaml_files

  end

  def read_resume_yaml
    return unless yaml_source_dir_exists?

    resume = {}
    yaml_files = filter_yaml_files


    if yaml_files.length == 0 or yaml_files.nil?
      raise StandardError.new 'Error: No YAML files in source directory that match list of includes' 
    end

    yaml_files.each do |f|
      yaml = YAML.load_file(f)
      resume.merge!(yaml)
    end

    return resume
  end

end

