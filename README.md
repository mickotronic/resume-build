# resume-build
My resume builder project, written in Ruby and HAML. This is the public project, which only contains example resume data. My actual resume data exists in a separate, private repository.

[View latest build artifact on GitLab Pages](https://mickotronic.gitlab.io/resume-build/)

# Problem to solve
Over the years, you end up with a lot of resume documents on your filesystem, and in your email. As the data and formatting exist in the same document, they are each their own source of truth. This can be incredibly annoying.

# Solution
This project is the public component of my resume project. It takes YAML data files, and outputs HTML and CSS assets to serve as a web page. There is a default HAML and CSS file used to generate and style the HTML file. The default HAML file uses the [paper-css](https://github.com/cognitom/paper-css) project to display the resulting page as an A4 page.

I wanted to keep this project public so others can view, contribute or fork, while keeping my personal data private. I also wanted **maximum** separation between my data, how the document is generated, how it is styled.

# Why Ruby and HAML?
A resume will be a document that I will use for my entire career. The lifetime of this document will span decades. I want to pick a format and workflow that can work for a large portion of that timeframe, without much change.

I tried and failed on a number of different options, and this has so far been the easiest. I chose Ruby because I am reading a lot of Ruby in my day-job, and I wanted to get more experience using it. I also like the ability to use HTML templates, and HAML does this nicely.

Even if somehow Ruby and HAML are no longer used or maintained in a very distant future, my data is stored separately and in a format that can be easily parsed in other languages.


# Why Git?
Version control in traditional word processing programs leaves a lot to be desired. Git provides excellent version control, is the industry standard, and will be around for the long haul.

Git branching is the perfect way to preserve history for different resume versions over time.

# How to get started
Once you clone this project, you will need to ensure you create a new private project to contain your personal data.

To populate your private project with the public `data` files to use as a starting point:

```
mkdir private-data

cd private-data

# Download tar of public project
wget https://gitlab.com/mickotronic/resume-build/-/archive/main/resume-build-main.tar -P /tmp

# Untar `data` directory to private project
tar -xvf /tmp/resume-build-main.tar --strip-components=2 resume-build-main/data
```

# How do I run this thing?

```
ruby main.rb
```

# I want to use my own data files
Cool! Please add a `config.yml` file in the root of the `resume-build` clone, and add the full directory path to the directory containing your private YAML files:

```
echo 'yaml_source: "/full/path/to/yaml/source/dir/"' > config.yml
```

By default, all YAML files in the above-configured directory will be read. You can use the `yaml_source_include` configuration option to whitelist specific YAML files in the `yaml_source` directory:

```
echo 'yaml_source_include: ["resume.yml"]' > config.yml
```

It's your choice whether you want to have all your YAML in one file, or split into multiple files.

# I want to use my own HAML templates
Cool! Add the template to the `templates` directory. Then, add the following to the `config.yml` file:

```
echo 'template: "templates/my-awesome-template.haml"' > config.yml
```

# I want to use my own stylesheets
Cool! Add the stylesheet to the `stylesheets` directory. Then, add the following to the `config.yml` file:

```
echo 'stylesheet: "stylesheets/my-awesome-stylesheet.css"' > config.yml
```

# Can I deploy this to GitLab Pages?
Yes! This project has a basic `.gitlab-ci.yml` file that deploys to GitLab Pages. Its purpose is to ensure the project generates the web assets correctly based on the default template and stylesheet.

I have a similar `.gitlab-ci.yml` file in my private project, however it clones this project, adds configuration to use my personal files, and runs the script:

```
stages:
  - build
  - review
  - deploy

image: ruby:3.0

build:
  stage: build
  before_script:
    - |
      git clone https://gitlab.com/mickotronic/resume-build.git
      cd resume-build
      bundle install
  script:
    - |
      echo "yaml_source: \"/builds/$CI_PROJECT_PATH/\"" > config.yml
      ruby main.rb
      cd ..
      cp -r resume-build/public/ ./public/
  artifacts:
    paths:
      - public/

pages:
  stage: deploy
  script:
    - ls -laR public/
  artifacts:
    paths:
      - public/
  only:
    - main
```
